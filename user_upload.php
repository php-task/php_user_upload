<?php

$short_help = "
Usage:
php user_upload.php --file csv_file_name [--create_table] [--dry_run] -u mysql_user_name -p mysql_password -h mysql_host [--help]

eg:
php user_upload.php --file './users.csv' -u root -p '' -h localhost --db users --dry_run
eg: creating table
php uu1.php -u root -p 'root' -h localhost --db users1 --create_table
eg: dry run
php uu1.php -u root -p 'root' -h localhost --db users1 --dry_run

";

$info = "$short_help what would you like to do?"."\n"
    ."--file user.csv for data to be parsed and insert in to DB"."\n".
    "--create_table to create database table"."\n".
    "--dry_run for data to be parsed "."\n".
    "-u mysql username"."\n".
    "-p MySQL password"."\n".
    "-h  MySQL host"."\n".
    "--db  MySQL Database"."\n".
    "--help  for all menue."."\n";
    
///php args1.php --file csv_file_name --create_table --dry_run -u mysql_user_name -p mysql_password -h mysql_host --db mysql_database --help

$shortopts  = "";
$shortopts .= "u:";  // Required value, -u Mysql username
$shortopts .= "p:";  // Required value, -p Mysql password
$shortopts .= "h:";  // Required value, -h Mysql host
//$shortopts .= "v::"; // Optional value, 
//$shortopts .= "abc"; // These options do not accept values

$longopts  = array(
	"db:",     // Required value, --file
    "file:",     // Required value, --file
    "create_table::",    // Optional value, --create_table
    "dry_run::",    // Optional value, --dry_run
    "help::",    // Optional value, --help
    "option",        // No value
    "opt",           // No value
);

$options = getopt($shortopts, $longopts);
//var_dump($options);

$arg_is_create_table = false;
$i="create_table";
if( array_key_exists($i, $options) ){
	$arg_is_create_table = true;
}

$arg_is_dry_run = false;
$i="dry_run";
if( array_key_exists($i, $options) ){
	$arg_is_dry_run = true;
}
$arg_is_help = false;
$i="help";
if( array_key_exists($i, $options) ){
	$arg_is_help = true;
}
if($arg_is_help){
	echo $info;
	die();
}

$argstr_csv_file_loc = "";
$i="file";
if( array_key_exists($i, $options) ){
	$argstr_csv_file_loc = $options[$i];
}
else{
	die("--file csv_file_name required $short_help");
}

$argstr_umysql = "";
$i="u";
if( array_key_exists($i, $options) ){
	$argstr_umysql = $options[$i];
}
else{
	if(!$arg_is_dry_run){
		die("-u mysql username required $short_help");
	}
}

$argstr_pmysql = "";
$i="p";
if( array_key_exists($i, $options) ){
	$argstr_pmysql = $options[$i];
}
else{
	if(!$arg_is_dry_run){
		die("-p mysql password required $short_help");
	}
	
}

$argstr_hmysql = "";
$i="h";
if( array_key_exists($i, $options) ){
	$argstr_hmysql = $options[$i];
}
else{
	if(!$arg_is_dry_run){
		die("-h mysql host required $short_help");
	}	
}

$argstr_hmysql = "";
$i="h";
if( array_key_exists($i, $options) ){
	$argstr_hmysql = $options[$i];
}
else{
	die("-h mysql host required $short_help");
}


$argstr_db = "";
$i="db";
if( array_key_exists($i, $options) ){
	$argstr_db = $options[$i];
}
else{
	die("--db mysql database required $short_help");
}






$servername = "localhost";
$username = "root";
$password = "";
$table = "users";

$servername = $argstr_hmysql;
$username = $argstr_umysql;
$password = $argstr_pmysql;
$database = $argstr_db;
$table = "users";

 
if($arg_is_help){
echo $info;    
}


if($arg_is_create_table)
{
	//  echo "this line is for create table"."\n";
	// Create connection

	$conn = new mysqli($servername, $username, $password);
	// Create database
	$sql = "CREATE DATABASE IF NOT EXISTS ". $argstr_db;
	if ($conn->query($sql) === TRUE) {
	  echo "Database  success \n";
	} else {
	  echo "Error creating database: " . $conn->error;
	}

	 $conn = new mysqli($servername, $username, $password, $argstr_db);
	// Check connection
	if ($conn->connect_error) {
	  die("Connection failed: " . $conn->connect_error);
	}

	  $create_table = "CREATE TABLE  IF NOT EXISTS users (
		`name` varchar(500),
		`surname` varchar(500),
		`email` varchar(500) UNIQUE) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_unicode_ci
		";

	// $create_tbl = $conn->query($create_table);

	if ($conn->query($create_table) === TRUE) {
		  echo "Table created successfully \n";
		} else {
		  echo " Error creating table: " . $conn->error;
		}

	$conn->close();
	die("Table created. Exiting... \n");

}


if(!$arg_is_dry_run){
	 $conn = new mysqli($servername, $username, $password, $argstr_db);
	// Check connection
	if ($conn->connect_error) {
	  die("Connection failed: " . $conn->connect_error);
	}
}

$csv_file = "./users.csv"; // Name of your CSV file
$csv_file = $argstr_csv_file_loc;
if (file_exists($csv_file)) {
    //echo "The file $filename exists";
} else {
	die("$csv_file not found");
}
$csvfile = fopen($csv_file, 'r');
$theData = fgets($csvfile); //skip header
$i = 0;
while (!feof($csvfile))
{

   $csv_data[] = fgets($csvfile, 1000000);
   $csv_array = explode(",", $csv_data[$i]);
   $i++;
   
   // print_r($csv_array);
   $count=count($csv_array);
   if($count < 3 ){
	continue;
   }
   
   
   $name = ucfirst(  strtolower($csv_array[0] ));
   $surname = ucfirst( strtolower($csv_array[1] ));
   $email = trim($csv_array[2]);
   $email = filter_var($email, FILTER_VALIDATE_EMAIL);
  // print($is_email_valid);
   //print_r($insert_csv);
   
   $query = "INSERT INTO users(name,surname,email) VALUES('$name', '$surname', '$email')";
   //echo "$query\n";
   $str_csv_row="'$name', '$surname', '$email'";
   if($arg_is_dry_run){
		echo "$str_csv_row \n";
   }
   else{
	   if ($conn->query($query) === TRUE) {
		  echo "$str_csv_row Inserted successfully";
		} else {
		  echo " Error creating record: " . $conn->error ."\n";
		}
	}
	//continue;
   //$n=mysqli_query($query, $connect );
}
fclose($csvfile);

//echo "File data successfully imported to database!!";

if(!$arg_is_dry_run){
	$conn->close();
}



 
?>